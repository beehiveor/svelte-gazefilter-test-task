# svelte-gazefilter-test-task

*Front-end developer test task with svelte*

Code in this repo is cloned from [svelte template](https://github.com/sveltejs/template), so fork/clone/download, run `npm install` and `npm run dev` and start hacking!

If you are not familiar with Svelte, see [official tutorial](https://svelte.dev/tutorial/). Required topics:

- Introduction: Basics, Adding data, Dynamic Attributes, Making an App;
- Reactivity: Assignments, Declarations, Statements;
- Logic: *if/else/else if*, *await* blocks;
- Lifecycle: onMount, onDestroy;
- Bindings: This.

## Task description

Recreate [demo SPA](https://gazefilter.web.app) using [Svelte](https://svelte.dev).

> recreate just functionality, fancy style is optional (but welcome)

Features to implement:

1. **initialization**:

    - [ ] install `gazefilter` via `npm`;
    - [ ] use [`rollup-plugin-copy`](https://www.npmjs.com/package/rollup-plugin-copy) to copy `gazefilter.wasm` to `public` directory;
    - [ ] library is loaded on button click ("open" button in the demo);
    - [ ] progress bar for loading;
    - [ ] show alert on any occured error and allow to retry;
    - [ ] after successfull initialization button disappears.

    *see* [*gazefilter: Getting started*](https://beehiveor.gitlab.io/gazefilter/readme.html).

2. **connection to device**:

    - [ ] connect to first available device right after initialization;
    - [ ] show alert on any occured error and allow to retry;
    - [ ] user can disconnect/connect.

3. **tracking visualization**:

    - [ ] add [HTMLCanvasElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement) to UI;
    - [ ] render canvas after successfull `gazefilter` initialization.

    *see* [*gazefilter: visualization API*](https://beehiveor.gitlab.io/gazefilter/visualize.html).


## References

- [gazefilter documentation](https://beehiveor.gitlab.io/gazefilter)
- [svelte template](https://github.com/sveltejs/template)



